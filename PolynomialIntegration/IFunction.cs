﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialIntegration
{
    public interface IFunction
    {
        /// <summary>
        /// Zwraca wartość funkcji dla podanego argumentu.
        /// </summary>
        /// <param name="argument">Argument funkcji</param>
        /// <returns>Wartość funkcji</returns>
        double GetValue(double argument);
    }
}
