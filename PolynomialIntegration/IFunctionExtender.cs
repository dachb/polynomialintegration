﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialIntegration
{
    /// <summary>
    /// Rozszerza klasy interfejsu IFunction o dodatkowe funkcjonalności.
    /// </summary>
    public static class IFunctionExtender
    {
        const double epsilon = 1e-8;

        /// <summary>
        /// Zwraca całkę z danej funkcji na danym przedziale, obliczoną złożoną kwadraturą
        /// Simpsona z podziałem na n podprzedziałów.
        /// </summary>
        /// <param name="func">Funkcja, z której całka ma być obliczona</param>
        /// <param name="a">Lewy kraniec przedziału</param>
        /// <param name="b">Prawy kraniec przedziału</param>
        /// <param name="n">Ilość podprzedziałów</param>
        /// <returns>Wartość całki na zadanym przedziale</returns>
        public static double Integrate(this IFunction func, double a, double b, int n)
        {
            // nieprawidłowa ilość podprzedziałów
            if (n <= 0)
                throw new ArgumentException("Liczba podprzedziałów musi być dodatnia");
            // zły przedział całkowania
            if (!func.IsValidArgument(a) || !func.IsValidArgument(b))
                throw new ArgumentException("Nieprawidłowy przedział całkowania");

            // długość podprzedziału
            double h = (b - a) / n;
            // jeśli przedział jest b. krótki, zwracamy 0
            if (Math.Abs(h) < epsilon)
                return 0.0;
            
            double x = a, y = a + h;    // x - lewy, y - prawy koniec podprzedziału
            double integral = 0;        // całkowita wartość całki
            double subintegral = 0;     // wartość całki na danym podprzedziale
            for (int i = 0; i < n; i++)
            {
                // kwadratura Simpsona
                subintegral = func.GetValue(x);
                subintegral += 4 * func.GetValue((x + y) / 2);
                subintegral += func.GetValue(y);
                integral += h * subintegral / 6;
                // przesunięcie przedziału
                x += h;
                y += h;
            }
            return integral;
        }

        /// <summary>
        /// Sprawdza, czy argument funkcji jest dozwolony (czy jest liczbą)
        /// </summary>
        /// <param name="func">Funkcja, na której rzecz metoda rozszerzająca 
        /// jest wykonywana</param>
        /// <param name="argument">Sprawdzany argument</param>
        /// <returns>true, jeśli argument jest prawidłowy</returns>
        public static bool IsValidArgument(this IFunction func, double argument)
        {
            return !double.IsInfinity(argument) && !double.IsNaN(argument);
        }
    }
}
