﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialIntegration
{
    /// <summary>
    /// Reprezentuje wielomian.
    /// </summary>
    public class Polynomial : IFunction
    {
        /// <summary>
        /// Stopień wielomianu
        /// </summary>
        int _degree;
        /// <summary>
        /// Współczynniki wielomianu, w kolejności rosnących potęg
        /// </summary>
        double[] _coefficients;

        /// <summary>
        /// Tworzy obiekt wielomianu o podanym stopniu i współczynnikach.
        /// </summary>
        /// <param name="degree">Stopień wielomianu</param>
        /// <param name="coefficients">Tablica współczynników, w kolejności malejących potęg</param>
        public Polynomial(int degree, double[] coefficients)
        {
            // nieprawidłowy stopień wielomianu
            if (degree < 0)
                throw new ArgumentException("Nieprawidłowy stopień wielomianu");
            // niezgodna liczba współczynników i stopnie
            if (degree + 1 != coefficients.Length)
                throw new ArgumentException("Liczba współczynników niezgodna ze stopniem wielomianu");
            _degree = degree;
            // odwrócenie kolejności w tablicy - dla wygody działania
            _coefficients = coefficients.Reverse().ToArray();
        }

        /// <summary>
        /// Oblicza wartość wielomianu dla danego argumentu.
        /// </summary>
        /// <param name="argument">Argument funkcji</param>
        /// <returns>Wartość funkcji</returns>
        public double GetValue(double argument)
        {
            // argument nie jest liczbą
            if (!this.IsValidArgument(argument))
                throw new ArgumentException("Argument funkcji nie jest liczbą");
            // wartość funkcji obliczana jest schematem Hornera w czasie O(n)
            double value = _coefficients[_degree];
            for (int i = _degree - 1; i >= 0; i--)
            {
                value *= argument;
                value += _coefficients[i];
            }
            return value;
        }
    }
}
