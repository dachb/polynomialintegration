﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolynomialIntegration
{
    class Program
    {
        /// <summary>
        /// Główna funkcja programu.
        /// </summary>
        /// <param name="args">Tablica argumentów wejściowych</param>
        public static void Main(string[] args)
        {
            int n, s;
            double k, l;
            double[] coefficients;
            try
            {
                // próba załadowania argumentów
                LoadArguments(args, out n, out k, out l, out s, out coefficients);
                // próba stworzenia obiektu wielomianu i zainicjowania go
                Polynomial p = new Polynomial(n, coefficients);
                double integral = p.Integrate(k, l, s);
                // wypisanie wyniku
                Console.WriteLine("Całka wynosi {0}", integral);
            }
            catch (Exception ex)
            {
                // szczegóły błędu
                if (ex is IndexOutOfRangeException)
                    Console.WriteLine("Błąd: Niewłaściwa ilość parametrów wejściowych.");
                else if (ex is FormatException)
                    Console.WriteLine("Błąd: Nieprawidłowy format danych wejściowych.");
                else
                    Console.WriteLine("Błąd: " + ex.Message);
                // drukujemy informacje o użyciu programu
                Usage();
                return;
            }
            return;
        }

        /// <summary>
        /// Ładuje wartości z argumentów wejściowych do zmiennych
        /// </summary>
        /// <param name="args">Tablica argumentów wejściowych</param>
        /// <param name="n">Liczba podprzedziałów</param>
        /// <param name="k">Lewy kraniec przedziału</param>
        /// <param name="l">Prawy kraniec przedziału</param>
        /// <param name="s">Liczba podprzedziałów w metodzie Simpsona</param>
        /// <param name="coefficients">Tablica współczynników</param>
        private static void LoadArguments(string[] args, out int n, out double k, out double l, out int s, out double[] coefficients)
        {
            try
            {
                // przypisanie argumentów z początku listy
                n = int.Parse(args[0]);
                k = double.Parse(args[1]);
                l = double.Parse(args[2]);
                s = int.Parse(args[3]);
                // współczynniki - zainicjalizowanie tablicy i przepisanie
                coefficients = new double[args.Length - 4];
                for (int i = 4; i < args.Length; i++)
                    coefficients[i - 4] = double.Parse(args[i]);
            }
            catch (Exception)
            {
                // w razie wyjątku rzucamy go wyżej
                throw;
            }
        }

        /// <summary>
        /// Wyświetla informacje o użyciu programu.
        /// </summary>
        private static void Usage()
        {
            Console.WriteLine("UŻYCIE:\tPolynomialIntegration.exe n k l s [wsp...]");
            Console.WriteLine("* n:\tStopień wielomianu (liczba całkowita, nieujemna)");
            Console.WriteLine("* k,l:\tPoczątek i koniec przedziału całkowania (liczba rzeczywista)");
            Console.WriteLine("* s:\tLiczba podprzedziałów (liczba całkowita, dodatnia)");
            Console.WriteLine("* wsp:\tWspółczynniki wielomianu, w kolejności malejących potęg (liczby rzeczywiste)");
            return;
        }
    }
}
