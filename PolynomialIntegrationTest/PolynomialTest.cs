﻿using System;
using System.Collections.Generic;
using PolynomialIntegration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PolynomialIntegrationTest
{
    [TestClass]
    public class PolynomialTest
    {
        /// <summary>
        /// Zadana dokładność
        /// </summary>
        const double epsilon = 1e-8;

        /// <summary>
        /// Definicja testu:
        /// Dla ujemnego stopnia wielomianu
        ///     program zgłasza wyjątek typu ArgumentException.
        /// </summary>
        [TestMethod]
        [TestCategory("Polynomial")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_ForNegativePolynomialDeg_ThrowsException()
        {
            int degree = -1;
            double[] coefficients = new double[0];

            Polynomial p = new Polynomial(degree, coefficients);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla rozmiaru tablicy współczynników niezgodnego ze stopniem wielomianu
        ///     program zgłasza wyjątek typu ArgumentException.
        /// </summary>
        [TestMethod]
        [TestCategory("Polynomial")]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_ForIncorrectArraySize_ThrowsException()
        {
            int degree = 3;
            double[] coefficients = new double[] { 1.0, -2.0 };

            Polynomial p = new Polynomial(degree, coefficients);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla zainicjalizowanego wielomianu stopnia 0
        ///     metoda GetValue zwraca poprawną wartość wielomianu.
        /// </summary>
        [TestMethod]
        [TestCategory("Polynomial")]
        public void GetValue_ForZeroDegPolynomial_ReturnsValue()
        {
            int degree = 0;
            double[] coefficients = new double[] { -1.0 };
            double arg = 2.0;

            Polynomial p = new Polynomial(degree, coefficients);

            Assert.AreEqual(coefficients[0], p.GetValue(arg));
            Assert.AreEqual(coefficients[0], p.GetValue(-arg));
        }

        /// <summary>
        /// Definicja testu:
        /// Dla zainicjalizowanego wielomianu stopnia wyższego niż 0
        ///     metoda GetValue zwraca poprawną wartość wielomianu.
        /// </summary>
        [TestMethod]
        [TestCategory("Polynomial")]
        public void GetValue_ForHigherDegPolynomial_ReturnsValue()
        {
            int degree = 2;
            double[] coefficients = new double[] { 1.0, -2.0, 1.0 };
            double arg = -4.0;

            Polynomial p = new Polynomial(degree, coefficients);

            Assert.AreEqual(25.0, p.GetValue(arg));
        }

        /// <summary>
        /// Definicja testu:
        /// Dla argumentu funkcji będącego nieskończonością
        ///     metoda GetValue zgłasza wyjątek typu ArgumentException.
        /// </summary>
        [TestMethod]
        [TestCategory("Polynomial")]
        [ExpectedException(typeof(ArgumentException))]
        public void GetValue_ForInfinityArgument_ThrowsException()
        {
            int degree = 0;
            double[] coefficients = new double[] { -1.0 };

            Polynomial p = new Polynomial(degree, coefficients);

            p.GetValue(double.PositiveInfinity);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla przedziału całkowania o długości 0
        ///     metoda Integrate zwraca wynik 0.
        /// </summary>
        [TestMethod]
        [TestCategory("Integrate")]
        public void Integrate_ForZeroWidthInterval_ReturnsZero()
        {
            int degree = 1;
            double[] coefficients = new double[] { 2.5, 3.5 };
            Polynomial p = new Polynomial(degree, coefficients);

            double result = p.Integrate(1.0, 1.0, 5);

            Assert.IsTrue(Math.Abs(result) < epsilon);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla wielomianu stopnia 0
        ///     metoda Integrate zwraca dokładny wynik (a_0 * (b - a))
        /// </summary>
        [TestMethod]
        [TestCategory("Integrate")]
        public void Integrate_ForZeroDegPolynomial_ReturnsIntegral()
        {
            int degree = 0;
            double[] coefficients = new double[] { -2.0 };
            Polynomial p = new Polynomial(degree, coefficients);
            double a = 5.0;
            double b = 10.0;

            double expected = coefficients[0] * (b - a);
            double actual = p.Integrate(a, b, 5);

            Assert.IsTrue(Math.Abs(expected - actual) < epsilon);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla wielomianu stopnia 2
        ///     metoda Integrate zwraca dokładny wynik.
        /// </summary>
        [TestMethod]
        [TestCategory("Integrate")]
        public void Integrate_ForCubicPolynomial_ReturnsExact()
        {
            int degree = 2;
            List<double> coefficients = new List<double> { -1.0, 3.0, -2.0 };
            Polynomial p = new Polynomial(degree, coefficients.ToArray());
            double a = -3.0;
            double b = 4.0;
            for (int i = 0; i < coefficients.Count - 1; i++)
                coefficients[i] /= (degree + 1 - i);
            coefficients.Add(0.0);
            Polynomial p_int = new Polynomial(degree + 1, coefficients.ToArray());

            double expected = p_int.GetValue(b) - p_int.GetValue(a);
            double actual = p.Integrate(a, b, 10);

            Assert.IsTrue(Math.Abs(expected - actual) < epsilon);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla nieprawidłowej liczby podprzedziałów
        ///     metoda Integrate zgłasza wyjątek typu ArgumentException.
        /// </summary>
        [TestMethod]
        [TestCategory("Integrate")]
        [ExpectedException(typeof(ArgumentException))]
        public void Integrate_ForInvalidSubintervalCount_ThrowsException()
        {
            int degree = 0;
            double[] coefficients = new double[] { 5.5 };
            Polynomial p = new Polynomial(degree, coefficients);

            p.Integrate(-5.0, 5.0, 0);
        }

        /// <summary>
        /// Definicja testu:
        /// Dla nieprawidłowego przedziału całkowania
        ///     metoda Integrate zgłasza wyjątek typu ArgumentException.
        /// </summary>
        [TestMethod]
        [TestCategory("Integrate")]
        [ExpectedException(typeof(ArgumentException))]
        public void Integrate_ForInvalidIntervalBounds_ThrowsException()
        {
            int degree = 0;
            double[] coefficients = new double[] { 2.0 };
            Polynomial p = new Polynomial(degree, coefficients);

            p.Integrate(double.NegativeInfinity, 2, 10);
        }
    }
}
