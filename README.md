# PolynomialIntegration #

Program konsolowy służący do numerycznego obliczania całki z wielomianu n-tego stopnia z użyciem złożonej kwadratury Simpsona.

### Składnia użycia ###

```
PolynomialIntegration.exe n k l s [wsp...]
```
* ```n``` - stopień wielomianu; liczba całkowita, nieujemna
* ```k``` - początek przedziału całkowania; liczba rzeczywista
* ```l``` - koniec przedziału całkowania; liczba rzeczywista
* ```s``` - liczba podprzedziałów w metodzie Simpsona; liczba całkowita, dodatnia
* ```wsp``` - współczynniki wielomianu, w kolejności malejących potęg; ```n + 1``` liczb rzeczywistych

### Przykładowe wywołanie ###
```
X:\PolynomialIntegration>PolynomialIntegration.exe 2 -3 2 10 1 -5 4
Całka wynosi 44,1666666666667
```

### Zawartość repozytorium ###
W repozytorium znajduje się kod źródłowy programu oraz zestaw testów jednostkowych.